import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.*;
import java.util.Locale;
import java.util.Scanner;


public class MiniballTest {

	@Test
	public void test_almost_cospherical_points_3()
	{
		Miniball mb;
		try {
			mb = computeFromFile(getClass().getResourceAsStream("data/almost_cospherical_points_3.data"));
		}
		catch(IOException e) {
			assert(false);
			return;
		}
		double[] expectedCenter = {
			-1.56087201342490318e-12, -4.71446026502953592e-12, 3.67580472387244665e-12
		};
		assertEquals(10000, mb.size());
		assertAlmostEquals(expectedCenter, mb.center());
		assertAlmostEquals(1.00000000015068569e+00, mb.squaredRadius());
	}

	@Test
	public void test_cocircular_points_large_radius_2() throws IOException
	{
		Miniball mb = computeFromFile(getClass().getResourceAsStream("data/cocircular_points_large_radius_2.data"));
		double[] expectedCenter = {
			2.25917732813639420e-08, 2.66532983586183870e-08
		};
		assertEquals(13824, mb.size());
		assertAlmostEquals(expectedCenter, mb.center());
		assertAlmostEquals(1.12830550249511283e+18, mb.squaredRadius());
	}

	@Test
	public void test_cocircular_points_small_radius_2() throws IOException
	{
		Miniball mb = computeFromFile(getClass().getResourceAsStream("data/cocircular_points_small_radius_2.data"));
		double[] expectedCenter = {
			0, 0
		};
		assertEquals(6144, mb.size());
		assertAlmostEquals(expectedCenter, mb.center());
		assertAlmostEquals(3.72870291637512500e+15, mb.squaredRadius());
	}

	@Test
	public void test_longitude_latitude_model_3() throws IOException
	{
		Miniball mb = computeFromFile(getClass().getResourceAsStream("data/longitude_latitude_model_3.data"));
		double[] expectedCenter = {
			1.09529643705128193e-12, 7.76487134128295763e-12, 2.09782485152182732e-16
		};
		assertEquals(10000, mb.size());
		assertAlmostEquals(expectedCenter, mb.center());
		assertAlmostEquals(1.00000000015833201e+00, mb.squaredRadius());
	}

	@Test
	public void test_random_points_3() throws IOException
	{
		Miniball mb = computeFromFile(getClass().getResourceAsStream("data/random_points_3.data"));
		double[] expectedCenter = {
			1.61093103034421342e-02, -3.39403799912667802e-03, -3.31378412407934593e-03
		};
		assertEquals(10000, mb.size());
		assertAlmostEquals(expectedCenter, mb.center());
		assertAlmostEquals(6.86700124388323507e-01, mb.squaredRadius());
	}

	Miniball computeFromFile(InputStream s) throws IOException
	{
		PointSet pts = pointsFromStream(s);
		Miniball mb = new Miniball(pts);
		System.out.println(mb);
		return mb;
	}

	public final static void assertAlmostEquals(double[] expected, double[] actual)
	{
		for (int i = 0; i < expected.length; ++i)
			assertAlmostEquals(expected[i], actual[i]);
	}

	public final static void assertAlmostEquals(double expected, double actual)
	{
		assertTrue(Math.abs(expected - actual) < 1e-5);
	}

	public static final ArrayPointSet pointsFromStream(InputStream s)
	{
		try
		{
			Reader r = new InputStreamReader(new BufferedInputStream(s), "UTF-8");
			Scanner in = new Scanner(r).useLocale(Locale.US);
			try
			{
				final int n = in.nextInt();
				final int d = in.nextInt();
				final ArrayPointSet pts = new ArrayPointSet(d, n);
				for (int i = 0; i < n; ++i)
					for (int j = 0; j < d; ++j)
						pts.set(i, j, in.nextDouble());
				return pts;
			}
			finally
			{
				try
				{
					r.close();
				}
				catch (IOException e)
				{
					throw new RuntimeException("Could not read points.", e);
				}
			}
		}
		catch (UnsupportedEncodingException e)
		{
			throw new RuntimeException("Unknown encoding.", e);
		}
	}
}
